package cz.cvut.fit.sassfili.services;

import cz.cvut.fit.sassfili.dto.SupermarketCreateDTO;
import cz.cvut.fit.sassfili.dto.SupermarketDTO;
import cz.cvut.fit.sassfili.entities.Supermarket;
import cz.cvut.fit.sassfili.exceptions.MissingSupermarketException;
import cz.cvut.fit.sassfili.repositories.SupermarketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SupermarketService {

    private final SupermarketRepository supermarketRepository;

    @Autowired
    public SupermarketService(SupermarketRepository supermarketRepository) {
        this.supermarketRepository = supermarketRepository;
    }

    public SupermarketDTO create(SupermarketCreateDTO supermarketCreateDTO){
        return toDTO(supermarketRepository.save(new Supermarket(supermarketCreateDTO.getName())));
    }

    private SupermarketDTO toDTO(Supermarket supermarket){
        return new SupermarketDTO(supermarket.getId(), supermarket.getName());
    }

    @Transactional
    public SupermarketDTO update(int id, SupermarketCreateDTO supermarketCreateDTO) throws Exception{
        Optional<Supermarket> optionalSupermarket = supermarketRepository.findById(id);
        if(optionalSupermarket.isEmpty())
            throw new MissingSupermarketException();
        Supermarket supermarket = optionalSupermarket.get();
        supermarket.setName(supermarketCreateDTO.getName());
        return toDTO(supermarket);
    }

    public List<SupermarketDTO> findAll(){
        return supermarketRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Supermarket> findByIds(List<Integer> ids){
        return supermarketRepository.findAllById(ids);
    }

    public Optional<Supermarket> findById(int id){
        return supermarketRepository.findById(id);
    }

    public Optional<SupermarketDTO> findByIdAsDTO(int id){
        return toDTO(supermarketRepository.findById(id));
    }

    private Optional<SupermarketDTO> toDTO(Optional<Supermarket> optionalSupermarket){
        if(optionalSupermarket.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalSupermarket.get()));
    }


}
