package cz.cvut.fit.sassfili.services;


import cz.cvut.fit.sassfili.dto.AlcoholCreateDTO;
import cz.cvut.fit.sassfili.dto.AlcoholTypeCreateDTO;
import cz.cvut.fit.sassfili.dto.AlcoholTypeDTO;
import cz.cvut.fit.sassfili.entities.AlcoholType;
import cz.cvut.fit.sassfili.exceptions.MissingAlcoholTypeException;
import cz.cvut.fit.sassfili.repositories.AlcoholTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AlcoholTypeService {
    private final AlcoholTypeRepository alcoholTypeRepository;

    @Autowired
    public AlcoholTypeService(AlcoholTypeRepository alcoholTypeRepository) {
        this.alcoholTypeRepository = alcoholTypeRepository;
    }

    public AlcoholTypeDTO create (AlcoholTypeCreateDTO alcoholTypeCreateDTO){
        return toDTO(alcoholTypeRepository.save(new AlcoholType(alcoholTypeCreateDTO.getName())));
    }

    @Transactional
    public AlcoholTypeDTO update(int id, AlcoholCreateDTO alcoholCreateDTO) throws Exception {
        Optional<AlcoholType> optionalAlcoholType = alcoholTypeRepository.findById(id);
        if(optionalAlcoholType.isEmpty())
            throw new MissingAlcoholTypeException();
        AlcoholType alcoholType = optionalAlcoholType.get();
        alcoholType.setName(alcoholCreateDTO.getName());
        return toDTO(alcoholType);
    }

    public List<AlcoholTypeDTO> findAll(){
        return alcoholTypeRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<AlcoholType> findByIds(List<Integer> ids){
        return alcoholTypeRepository.findAllById(ids);
    }

    public Optional<AlcoholType> findById(int id){
        return alcoholTypeRepository.findById(id);
    }

    public Optional<AlcoholTypeDTO> findByIDAsDTO(int id){
        return toDTO(alcoholTypeRepository.findById(id));
    }

    private AlcoholTypeDTO toDTO(AlcoholType alcoholType){
        return new AlcoholTypeDTO(alcoholType.getId(),alcoholType.getName());
    }

    private Optional<AlcoholTypeDTO> toDTO(Optional<AlcoholType> optionalAlcoholType){
        if(optionalAlcoholType.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalAlcoholType.get()));
    }

}
