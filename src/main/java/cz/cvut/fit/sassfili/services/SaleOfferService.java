package cz.cvut.fit.sassfili.services;

import cz.cvut.fit.sassfili.dto.SaleOfferCreateDTO;
import cz.cvut.fit.sassfili.dto.SaleOfferDTO;
import cz.cvut.fit.sassfili.dto.SupermarketDTO;
import cz.cvut.fit.sassfili.entities.Alcohol;
import cz.cvut.fit.sassfili.entities.SaleOffer;
import cz.cvut.fit.sassfili.entities.Supermarket;
import cz.cvut.fit.sassfili.exceptions.MissingAlcoholException;
import cz.cvut.fit.sassfili.exceptions.MissingSaleOfferException;
import cz.cvut.fit.sassfili.exceptions.MissingSupermarketException;
import cz.cvut.fit.sassfili.repositories.SaleOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class SaleOfferService {

    private final SaleOfferRepository saleOfferRepository;
    private final AlcoholService alcoholService;
    private final SupermarketService supermarketService;

    @Autowired
    public SaleOfferService(SaleOfferRepository saleOfferRepository,
                            AlcoholService alcoholService,
                            SupermarketService supermarketService) {
        this.saleOfferRepository = saleOfferRepository;
        this.alcoholService = alcoholService;
        this.supermarketService = supermarketService;
    }

    public SaleOfferDTO create(SaleOfferCreateDTO saleOfferCreateDTO) throws Exception{
        Optional<Alcohol> optionalAlcohol = alcoholService.findById(saleOfferCreateDTO.getAlcohol());
        if(optionalAlcohol.isEmpty())
            throw new MissingAlcoholException();

        Optional<Supermarket> optionalSupermarket = supermarketService.findById(saleOfferCreateDTO.getAlcohol());
        if(optionalSupermarket.isEmpty())
            throw new MissingSupermarketException();

        return toDTO(saleOfferRepository.save(
                new SaleOffer(
                        saleOfferCreateDTO.getStartDate(),
                        saleOfferCreateDTO.getEndDate(),
                        saleOfferCreateDTO.getPrice(),
                        optionalAlcohol.get(),
                        optionalSupermarket.get()
                )));
    }

    @Transactional
    public SaleOfferDTO update(int id, SaleOfferCreateDTO saleOfferCreateDTO) throws  Exception{
        Optional<SaleOffer> optionalSaleOffer = saleOfferRepository.findById(id);
        if(optionalSaleOffer.isEmpty())
            throw new MissingSaleOfferException();

        Optional<Alcohol> optionalAlcohol = alcoholService.findById(saleOfferCreateDTO.getAlcohol());
        if(optionalAlcohol.isEmpty())
            throw new MissingAlcoholException();

        Optional<Supermarket> optionalSupermarket = supermarketService.findById(saleOfferCreateDTO.getSupermarket());
        if(optionalSupermarket.isEmpty())
            throw new MissingSupermarketException();

        SaleOffer saleOffer = optionalSaleOffer.get();
        saleOffer.setStartDate(saleOfferCreateDTO.getStartDate());
        saleOffer.setEndDate(saleOfferCreateDTO.getEndDate());
        saleOffer.setPrice(saleOfferCreateDTO.getPrice());
        saleOffer.setAlcohol(optionalAlcohol.get());
        saleOffer.setSupermarket(optionalSupermarket.get());

        return toDTO(saleOffer);
    }

    private SaleOfferDTO toDTO(SaleOffer saleOffer){
        return new SaleOfferDTO(
                saleOffer.getId(),
                saleOffer.getStartDate(),
                saleOffer.getEndDate(),
                saleOffer.getPrice(),
                saleOffer.getAlcohol().getId(),
                saleOffer.getSupermarket().getId()
        );
    }

    private Optional<SaleOfferDTO> toDTO(Optional<SaleOffer> optionalSaleOffer) {
        if(optionalSaleOffer.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalSaleOffer.get()));
    }
}
