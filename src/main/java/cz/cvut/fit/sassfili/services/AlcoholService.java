package cz.cvut.fit.sassfili.services;

import cz.cvut.fit.sassfili.dto.AlcoholCreateDTO;
import cz.cvut.fit.sassfili.dto.AlcoholDTO;
import cz.cvut.fit.sassfili.entities.Alcohol;
import cz.cvut.fit.sassfili.entities.AlcoholType;
import cz.cvut.fit.sassfili.exceptions.MissingAlcoholException;
import cz.cvut.fit.sassfili.exceptions.MissingAlcoholTypeException;
import cz.cvut.fit.sassfili.repositories.AlcoholRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AlcoholService {
    private final AlcoholRepository alcoholRepository;
    private final AlcoholTypeService alcoholTypeService;


    @Autowired
    public AlcoholService(AlcoholRepository alcoholRepository, AlcoholTypeService alcoholTypeService) {
        this.alcoholRepository = alcoholRepository;
        this.alcoholTypeService = alcoholTypeService;
    }

    public AlcoholDTO create(AlcoholCreateDTO alcoholCreateDTO) throws MissingAlcoholTypeException{
        Optional<AlcoholType> optionalAlcoholType = alcoholTypeService.findById(alcoholCreateDTO.getAlcoholType());
        if(optionalAlcoholType.isEmpty())
            throw new MissingAlcoholTypeException();
        return toDTO(alcoholRepository.save(
                new Alcohol(
                    alcoholCreateDTO.getName(),
                    alcoholCreateDTO.getAlcohol_percentage(),
                    optionalAlcoholType.get()
                )));
    }

    @Transactional
    public AlcoholDTO update(int id, AlcoholCreateDTO alcoholCreateDTO) throws Exception{
        Optional<Alcohol> optionalAlcohol = alcoholRepository.findById(id);
        if(optionalAlcohol.isEmpty())
            throw new MissingAlcoholException();
        Optional<AlcoholType> optionalAlcoholType = alcoholTypeService.findById(alcoholCreateDTO.getAlcoholType());
        if(optionalAlcoholType.isEmpty())
            throw new MissingAlcoholTypeException();
        Alcohol alcohol = optionalAlcohol.get();
        alcohol.setName(alcoholCreateDTO.getName());
        alcohol.setAlcohol_percentage(alcoholCreateDTO.getAlcohol_percentage());
        alcohol.setAlcoholType(optionalAlcoholType.get());
        return toDTO(alcohol);
    }

    public List<AlcoholDTO> findAll(){
        return alcoholRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Alcohol> findByIds(List<Integer> ids){
        return alcoholRepository.findAllById(ids);
    }

    public Optional<AlcoholDTO> findByIDAsDTO(int id){
        return toDTO(alcoholRepository.findById(id));
    }

    public Optional<Alcohol> findById(int id){
        return alcoholRepository.findById(id);
    }

    private AlcoholDTO toDTO(Alcohol alcohol){
        return new AlcoholDTO(
                alcohol.getId(),
                alcohol.getName(),
                alcohol.getAlcohol_percentage(),
                alcohol.getAlcoholType().getId()
        );
    }

    private Optional<AlcoholDTO> toDTO(Optional<Alcohol> optionalAlcohol){
        if(optionalAlcohol.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalAlcohol.get()));
    }
}
