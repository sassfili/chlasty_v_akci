package cz.cvut.fit.sassfili.dto;

public class AlcoholTypeCreateDTO {
    private final String name;

    public AlcoholTypeCreateDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
