package cz.cvut.fit.sassfili.dto;

public class AlcoholTypeDTO {
    private final int id;
    private final String name;

    public AlcoholTypeDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
