package cz.cvut.fit.sassfili.dto;

import cz.cvut.fit.sassfili.entities.AlcoholType;

public class AlcoholCreateDTO {
    private final String name;
    private final double alcohol_percentage;
    private final Integer alcoholType;

    public AlcoholCreateDTO(String name, double alcohol_percentage, Integer alcoholType) {
        this.name = name;
        this.alcohol_percentage = alcohol_percentage;
        this.alcoholType = alcoholType;
    }

    public String getName() {
        return name;
    }

    public double getAlcohol_percentage() {
        return alcohol_percentage;
    }

    public Integer getAlcoholType() {
        return alcoholType;
    }
}
