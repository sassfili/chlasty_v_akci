package cz.cvut.fit.sassfili.dto;

import cz.cvut.fit.sassfili.entities.Alcohol;
import cz.cvut.fit.sassfili.entities.Supermarket;
import java.sql.Date;

public class SaleOfferDTO {
    private final int id;
    private final Date startDate;
    private final Date endDate;
    private final double price;
    private final Integer alcohol;
    private final Integer supermarket;

    public SaleOfferDTO(int id, Date startDate, Date endDate, double price, Integer alcohol, Integer supermarket) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.alcohol = alcohol;
        this.supermarket = supermarket;
    }

    public int getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public double getPrice() {
        return price;
    }

    public Integer getAlcohol() {
        return alcohol;
    }

    public Integer getSupermarket() {
        return supermarket;
    }
}
