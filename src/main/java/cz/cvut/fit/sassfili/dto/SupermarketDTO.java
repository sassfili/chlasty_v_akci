package cz.cvut.fit.sassfili.dto;

public class SupermarketDTO {
    private final int id;
    private final String name;

    public SupermarketDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
