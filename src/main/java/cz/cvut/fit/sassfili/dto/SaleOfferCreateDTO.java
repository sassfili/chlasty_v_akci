package cz.cvut.fit.sassfili.dto;

import cz.cvut.fit.sassfili.entities.Alcohol;
import cz.cvut.fit.sassfili.entities.Supermarket;

import java.sql.Date;

public class SaleOfferCreateDTO {
    private final Date startDate;
    private final Date endDate;
    private final double price;
    private final Integer alcohol;
    private final Integer supermarket;

    public SaleOfferCreateDTO(Date startDate, Date endDate, double price, Integer alcohol, Integer supermarket) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.alcohol = alcohol;
        this.supermarket = supermarket;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public double getPrice() {
        return price;
    }

    public Integer getAlcohol() {
        return alcohol;
    }

    public Integer getSupermarket() {
        return supermarket;
    }
}
