package cz.cvut.fit.sassfili.dto;

public class SupermarketCreateDTO {
    private final String name;

    public SupermarketCreateDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
