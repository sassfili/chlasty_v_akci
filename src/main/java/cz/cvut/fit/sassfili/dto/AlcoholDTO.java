package cz.cvut.fit.sassfili.dto;

import cz.cvut.fit.sassfili.entities.AlcoholType;


public class AlcoholDTO {
    private final int id;
    private final String name;
    private final double alcohol_percentage;
    private final Integer alcoholType;

    public AlcoholDTO(int id, String name, double alcohol_percentage, Integer alcoholType) {
        this.id = id;
        this.name = name;
        this.alcohol_percentage = alcohol_percentage;
        this.alcoholType = alcoholType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getAlcohol_percentage() {
        return alcohol_percentage;
    }

    public Integer getAlcoholType() {
        return alcoholType;
    }
}
