package cz.cvut.fit.sassfili;

public class WelcomeDrink {
    private final Long id;
    private final String message;/*
    private final String beer =
        "@@@@@@@@@@@@@@\n" +
        "|@@@o...@.o..|.____.\n" +
        "|.@...o....o.|/....|\n" +
        "|...o...o...o|...../\n" +
        "|...o......o.|..../\n" +
        "|..o...o..o..|___/\n" +
        "|____________|\n";

    public String getBeer() {
        return beer;
    }
*/
    public WelcomeDrink(Long id, String message) {
        this.id = id;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
