package cz.cvut.fit.sassfili.entities;


import javax.persistence.*;
import java.sql.Date;

@Entity(name = "SaleOffer")
@Table(name = "SaleOffer")
public class SaleOffer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saleoffer_id", nullable = false)
    private int id;

    @Column(name = "saleoffer_startdate", nullable = false)
    private Date startDate;

    @Column(name = "saleoffer_enddate", nullable = false)
    private Date endDate;

    @Column(name = "saleoffer_price", nullable = false)
    private double price;

    @ManyToOne(optional = false)
    @JoinColumn(name = "alcohol_id", nullable = false)
    private Alcohol alcohol;

    @ManyToOne(optional = false)
    @JoinColumn(name = "supermarket_id", nullable = false)
    private Supermarket supermarket;

    public SaleOffer(Date startDate, Date endDate, double price, Alcohol alcohol, Supermarket supermarket) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.alcohol = alcohol;
        this.supermarket = supermarket;
    }

    public int getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Alcohol getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Alcohol alcohol) {
        this.alcohol = alcohol;
    }

    public Supermarket getSupermarket() {
        return supermarket;
    }

    public void setSupermarket(Supermarket supermarket) {
        this.supermarket = supermarket;
    }
}
