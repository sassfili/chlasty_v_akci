package cz.cvut.fit.sassfili.entities;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity(name = "supermarket")
@Table(name = "supermarket")
public class Supermarket {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "supermarket_id", nullable = false)
    private int id;

    @Column(name = "supermarket_name", nullable = false)
    private String name;

    public Supermarket() {
    }

    public int getId() {
        return id;
    }

    public Supermarket(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
