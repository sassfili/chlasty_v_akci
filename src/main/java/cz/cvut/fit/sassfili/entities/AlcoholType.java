package cz.cvut.fit.sassfili.entities;

import javax.persistence.*;

@Entity(name = "alcoholtype")
@Table(name = "alcoholtype")
public class AlcoholType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "alcoholtype_id", nullable = false)
    private int id;

    @Column(name = "alcoholtype_name", nullable = false)
    private String name;

    public AlcoholType() {
    }

    public AlcoholType(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
