package cz.cvut.fit.sassfili.entities;

import javax.persistence.*;

@Entity(name = "alcohol")
@Table(name = "alcohol")
public class Alcohol {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "alcohol_id", nullable = false)
    private int id;

    @Column(name = "alcohol_Name", nullable = false)
    private String name;

    @Column(name = "alcohol_percentage", nullable = false)
    private double alcohol_percentage;

    @ManyToOne( optional = false )
    @JoinColumn( name = "alcoholtype_id", nullable = false)
    private AlcoholType alcoholType;

    public Alcohol() {
    }

    public Alcohol(String name, double alcohol_percentage, AlcoholType alcoholType) {
        this.name = name;
        this.alcohol_percentage = alcohol_percentage;
        this.alcoholType = alcoholType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAlcohol_percentage() {
        return alcohol_percentage;
    }

    public void setAlcohol_percentage(double alcohol_percentage) {
        this.alcohol_percentage = alcohol_percentage;
    }

    public AlcoholType getAlcoholType() {
        return alcoholType;
    }

    public void setAlcoholType(AlcoholType alcoholType) {
        this.alcoholType = alcoholType;
    }
}
