package cz.cvut.fit.sassfili.repositories;

import cz.cvut.fit.sassfili.entities.Alcohol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlcoholRepository extends JpaRepository<Alcohol, Integer> {
}
