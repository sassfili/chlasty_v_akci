package cz.cvut.fit.sassfili.repositories;

import cz.cvut.fit.sassfili.entities.AlcoholType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlcoholTypeRepository extends JpaRepository<AlcoholType, Integer> {
}
