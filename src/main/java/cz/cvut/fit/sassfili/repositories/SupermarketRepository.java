package cz.cvut.fit.sassfili.repositories;

import cz.cvut.fit.sassfili.entities.Supermarket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupermarketRepository extends JpaRepository<Supermarket, Integer> {
}
