package cz.cvut.fit.sassfili.repositories;

import cz.cvut.fit.sassfili.entities.SaleOffer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleOfferRepository extends JpaRepository<SaleOffer, Integer> {
}
