package cz.cvut.fit.sassfili;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class WelcomeDrinkController {

    private static final String template = "Hello, %s, here is your beer!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/welcome")
    public WelcomeDrink welcome(@RequestParam(value="name", defaultValue="Pepik") String name) {
        return new WelcomeDrink(counter.incrementAndGet(), String.format(template, name));
    }
}
