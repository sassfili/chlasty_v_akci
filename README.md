# chlasty_v_akci

Aplikace bude fungovat podobně jako https://www.kupi.cz/slevy/alkohol 
, nebo jako https://www.instagram.com/chlasty_v_akci/
,tedy bude fungovat jako interaktivni leták se slevamy alkoholu.
Měla by sloužit žíznivým studentům, kteří mají hluboko do kapsy ušetřit 
nejaké finanční dispozice.

Aplikace bude nabízet různé možnosti filtrování alkoholu
, například podle prodejce (Albert, Kaufland), nebo podle druhu (pivo, vodka),
aplikace také bude monitorovat vývoj ceny alkoholu
a upozorňovat na výhodné nabídky.
